/**
 * Module forcing users to connect to localhost.
 * Aborts connection attempt otherwise.
 *
 * (c) 2010 N Lum <nol888@gmail.com>
 *
 */

#include "IRCSock.h"
#include "User.h"
#include "Server.h"
#include "znc.h"

class CForceLocalConnMod : public CModule
{
public:
	MODCONSTRUCTOR(CForceLocalConnMod) {}
	virtual ~CForceLocalConnMod()
	{
	}
	
	virtual EModRet OnIRCConnecting(CIRCSock* pIRCSock)
	{
		if (GetUser()->GetCurrentServer()->GetName() != "127.0.0.1") {
			GetUser()->PutStatus("Connections to hosts other than '127.0.0.1' disallowed by Rizon BNC TOS. Please change your server to '127.0.0.1'");
			return HALT;
		} else {
			return CONTINUE;
		}
	}
};

MODULEDEFS(CForceLocalConnMod, "force users to connect to localhost")
